module gl

    use const
    implicit none

contains

!Расчет значений полинома Лежандра степени n в точке z
    real function P(n,z) result(f)

        integer :: n
        real(mp) :: z
        real(mp) a, b
        integer i

        if(n==0) then
            f=1.0
        else if(n==1) then
            f=z
        else
            a=1.0; b=z
            do i=2,n
                f=b*z+float(i-1)*(b*z-a)/float(i)
                a=b; b=f
            end do
        end if

    end function P

!Расчет значений производной полинома Лежандра степени n в точке z
    real(mp) function dP(n, z) result(f)

        real(mp) z
        integer n

        f=n/(1-z*z)*(P(n-1, z)-z*P(n, z))

    end function dp

!Расчет корней полинома Лежандра методом Ньютона
    subroutine get_xk(n, xi)

        real(mp), dimension(:) :: xi
        real(mp) xii
        integer i, n

        do i=1, n
            xi(i)=cos((pi*(4*i-1))/(4*n+2))  !Начальное приближение
        enddo

        do i=1, n
            do while (1 .ne. 0)
                xii=xi(i)-P(n, xi(i))/dP(n, xi(i))
                if (abs(xii-xi(i)) .lt. eps) then
                    exit
                endif
                    xi(i)=xii
            enddo
        enddo

    end subroutine get_xk

!Расчет весов квадратуры
    subroutine get_w(n, xi, ci)

        real(mp), dimension(:) :: ci, xi
        integer n, i

        do i=1, n
            ci(i)=2/((1-xi(i)*xi(i))*dp(n, xi(i))*dp(n, xi(i)))
        enddo

    end subroutine get_w
 
end module gl
