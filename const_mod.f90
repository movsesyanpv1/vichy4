module const
    integer, parameter :: mp = 4
    real(mp), parameter :: const_a = 10
    real(mp), parameter :: pi = 4*atan(1.0) !$\pi = 4\tan^{-1}(1)$
    real(mp), parameter :: eps = 1e-6
    real(mp), parameter :: lb = 0
    real(mp), parameter :: hb = 1
end module const